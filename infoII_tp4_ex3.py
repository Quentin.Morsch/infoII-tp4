class File:
    def __init__(self):
        self.items = []
        self.priority = [] # 0 : low priority - 5 : high priority | highest priovrity goes to the end of the list (right)

    def enqueue(self, item, priority_level): #Sort the items by priority

        if priority_level == 0:
            self.items.insert(0, item)
            self.priority.insert(0, priority_level)


        else:
            inserted = False
            for i in range(len(self.priority)):
                if priority_level < self.priority[i]:
                    self.items.insert(i, item)
                    self.priority.insert(i, priority_level)
                    inserted = True
                    break
            if inserted == False:
                self.items.append(item)
                self.priority.append(priority_level)

        print(self.items)
        print(self.priority)

        #self.items.insert(0, item)

    def dequeue(self): #Beacause the list is sorted, we can remove the last element
        self.priority.pop()
        return self.items.pop()

    def is_empty(self):
        return (self.items == [])

    def size(self):
        return len(self.items)


def predetermine_sequence():
    file.enqueue("A", 3)
    file.enqueue("B", 1)
    file.dequeue()
    file.enqueue("C", 0)
    file.enqueue("D", 2)
    file.enqueue("E", 5)
    file.enqueue("F", 4)
    file.dequeue()

    print("\nThe sequence does the following actions :")
    print(" 1) Write A with priority level 3")
    print(" 2) Write B with priority level 1")
    print(" 3) Remove the last element")
    print(" 4) Write C with priority level 0")
    print(" 5) Write D with priority level 2")
    print(" 6) Write E with priority level 5")
    print(" 7) Write F with priority level 4")
    print(" 8) Remove the last element")


    print("\nAfter executing the sequence, the stack is : ")
    print(list(file.items))
    print("\nAfter executing the sequence, the priority level list is : ")
    print(list(file.priority))

def menu():
    choice = 0
    while choice != 7:
        print("\n 1) Display the stack \n 2) Write an element \n 3) Remove an element \n 4) Display the depth of the "
              "stack \n 5) Predetermined sequence \n 6) Exit")
        choice = int(input("\nEnter your choice: "))

        if choice == 1: #Display the stack
            if not file.items:
                print("\nThe stack is empty")
            else:
                print("\nThe stack is: ")
                print("Items list : ")
                print(list(file.items))
                print("\nPriority level list : ")
                print(list(file.priority))

        elif choice == 2: #Write an element
            element = input("\nEnter the element to write: ")
            priority = int(input("\nEnter the priority level of the element: "))
            file.enqueue(element, priority)
            print("The element has been written")

        elif choice == 3: #Remove an element
            if file.is_empty():
                print("\nThe stack is empty, nothing to remove")

            else:
                file.dequeue()
                print("\nThe last element has been removed")

        elif choice == 4: #Display the depth of the stack
            print("\nThe depth of the stack is: ")
            print(file.size())

        elif choice == 5: #Predifine sequence of actions
            print("\nThe sequence of actions has been performed")
            predetermine_sequence()


        elif choice == 6:
            print("\nEnd")
            break

        else:
            print("\nInvalid choice")




if __name__ == "__main__":
    file = File()
    menu()